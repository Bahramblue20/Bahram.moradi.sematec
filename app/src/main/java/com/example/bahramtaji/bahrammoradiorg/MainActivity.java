package com.example.bahramtaji.bahrammoradiorg;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnwifi, btnblue, btngps, btncamer, btnpalcepicker;
    TextView txtgps,txtblue,txtwifi;
    ImageView imgview;
    private Uri imageUri;
    private static final int TAKE_PICTURE = 1;
    private final static int PLACE_PICKER_REQUEST = 999;
    private BluetoothAdapter mBluetoothAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindcomponents();
        btnwifi.setOnClickListener(this);
        btnblue.setOnClickListener(this);
        btngps.setOnClickListener(this);
        btncamer.setOnClickListener(this);
        btnpalcepicker.setOnClickListener(this);
    }

    public void bindcomponents() {
        btnblue = (Button) findViewById(R.id.btnblue);
        btngps = (Button) findViewById(R.id.btngps);
        btnwifi = (Button) findViewById(R.id.btnwifi);
        btncamer = (Button) findViewById(R.id.btncamera);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        imgview = (ImageView) findViewById(R.id.imgview);
        btnpalcepicker = (Button) findViewById(R.id.btnplacepicker);
        txtblue=(TextView)findViewById(R.id.txtblue);
        txtwifi=(TextView)findViewById(R.id.txtwifi);
        txtgps=(TextView)findViewById(R.id.txtgps);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnwifi) {
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        }
        if (v.getId() == R.id.btnblue) {
            startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));

            //bluetooth();
        }
        if (v.getId() == R.id.btngps) {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
        if (v.getId() == R.id.btncamera) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, TAKE_PICTURE);
        }
        if (v.getId() == R.id.btnplacepicker) {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            try {
                // for activty
                startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
                // for fragment
                //startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == TAKE_PICTURE) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                //mImageView.setImageBitmap(imageBitmap);
                imgview.setImageBitmap(imageBitmap);

            }

        }
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }


    }
    //    public void bluetooth() {
//        boolean bluetoothOff = !mBluetoothAdapter.isEnabled();
//        if (bluetoothOff) {
//            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            startActivityForResult(enableBtIntent, 1);
//        } else {
//            mBluetoothAdapter.disable();
//           // bluetoothDisabled();
//            Toast.makeText(MainActivity.this, "Bluetooth disabled", Toast.LENGTH_LONG).show();
//        }
//    }


    @Override
    protected void onResume() {
        WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        BluetoothManager bluetoothManager = (BluetoothManager) (getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE));
        LocationManager gps = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        if (wifi.isWifiEnabled()) {
            txtwifi.setText("Wifi is On");
        } else txtwifi.setText("Wifi is off");
        if (bluetoothManager.getAdapter().isEnabled()) {
            txtblue.setText("Bluetooth is On");
        } else txtblue.setText("Blutooth is off");
        if (gps.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            txtgps.setText("Gps is On");
        } else txtgps.setText("Gps is off");

        super.onResume();

    }
}
